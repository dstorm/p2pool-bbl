from distutils.core import setup, Extension

bitblock_module = Extension('bitblock_subsidy', sources = ['bitblock_subsidy.cpp'])

setup (name = 'bitblock_subsidy',
       version = '1.0',
       description = 'Subsidy function for BitBlock',
       ext_modules = [bitblock_module])
