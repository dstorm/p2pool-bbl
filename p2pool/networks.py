from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(
    bitblock=math.Object(
        PARENT=networks.nets['bitblock'],
        SHARE_PERIOD=15, # seconds
        CHAIN_LENGTH=24*60*60//15, # shares
        REAL_CHAIN_LENGTH=24*60*60//15, # shares
        TARGET_LOOKBEHIND=60, # shares  //with that the pools share diff is adjusting faster, important if huge hashing power comes to the pool
        SPREAD=15, # blocks
        IDENTIFIER='ad72ef8595396852'.decode('hex'),
        PREFIX='84dc600edc4a8e2f'.decode('hex'),
        P2P_PORT=7888,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=7887,
        BOOTSTRAP_ADDRS='bbl.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-bbl',
        VERSION_CHECK=lambda v: True,
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
